# GitLab CI Unreal Files
In this repository the shared scripts, the launcher meta data file and two sample `.gitlab-ci.yml` files are stored. If you just want an automatic compile and deploy of your project to the CAVE (vrdemo or av006de) and the VRDev network drive, copy the `project.gitlab-ci.yml` and `launcher.meta` to your projects root, rename `project.gitlab-ci.yml` to `.gitlab-ci.yml` and set up a scheduled build in the project preferences. You can use the `UNREAL_VERSION` variable in said .yml configurations to define your required Unreal version.
If you need more information, have a look into the wiki [here](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/unreal-ci/-/wikis/Setting-up-the-Pipeline-for-your-Project)!

```mermaid
graph LR
    subgraph "Git"
    proj_git(Project)
    plug_git(Plugin)
    end

    subgraph "Preparation"
    gen(Project Generation)
    end

    subgraph "Building"
    proj(("#32;"))
    build_win(Build Windows)
    build_lnx(Build Linux)
    deploy_win(Deploy VRDev)
    deploy_lnx(Deploy CAVE)
    end

    proj_git --> proj
    proj --> build_win
    proj --> build_lnx
    plug_git --> gen
    gen --> proj
    build_win --> deploy_win
    build_lnx --> deploy_lnx

```

# Standalone Plugin Compilation
If you want to compile your plugin without giving a project to go with it, copy the `plugins.gitlab-ci.yml` and `launcher.meta` to to your projects root and rename `plugins.gitlab-ci.yml` to `.gitlab-ci.yml`.

Specific for these Plugins the Generation Stage is provided, which generates a project and adds your plugin and its dependencies to it. For this step, have a look at the `.gitlab-ci.yml` and read the documentation there.

## Customization
In the `.gitlab-ci.yml` references to the shared scripts of this repository are defined. If you need to change something of the premade pipeline, have a look into this file. 
You can alter/remove the stages and add extra jobs per stage. Predefined stages are at the moment the

    .Generate_Project_, .Build_Windows_, .Build_CentOS_, .Deploy_VRDev_, .Deploy_CAVE_, .Deploy_vrdemo_

stages, which are referenced by the ones in the sample files.

.Deploy_CAVE_ deploys to legacy av006de, whereas .Deploy_vrdemo deploys to the shared vrdemo file system

If you dont need specific jobs, for example a Linux build, then please consider to comment out the jobs in your local file to speed up the pipeline.

The `UNREAL_VERSION` variable can be used to adjust the required Unreal Version with which the project should be built.

The `CUSTOM_NDISPLAY_CONFIG variable` variable can be used to further adjust which ndisplay config the weblauncher passes to the launch scripts. Normally, the standard aixcave config for the respective `UNREAL_VERSION` is chosen. By specifying a name/path relative to `/home/vrdemo/configs/ndisplay/`, you can override this.

### Build Steps
For the predefined build steps you can change the `CLIENT_CONFIG` to either `DebugGame` or `Shipping` (Default). Additionally, you can add parameters to the build command by specifying the `EXTRA_ARGS` variable. If you need to change the whole command, manually "fork" the job by copying the script part to your local file and removing the extends line.

### Deploy Steps
The predefined deploy steps are a bit more tricky to alter. So just try not to.
The two predefined deploy stages are automatically deploying the builds (if successful) to */work/vrdemo/projects/ci* or  */home/av006de/demos_UNREAL/CI* (CAVE, CentOS, Deprecated) or */home/vrdev/UnrealCIDeploy* (VRDev, Win64).

For vrdemo deployment, there is an additional ssh command that checks whether there are more than 3 deployed builds per branch, and deletes the older ones automatically. On vrdemo, no automatic cleanup cronjob is running.

---

### Setting up additional build machines
Have a look into the wiki of this project [here](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/unreal-ci/-/wikis/Setting-up-Build-Machines)
