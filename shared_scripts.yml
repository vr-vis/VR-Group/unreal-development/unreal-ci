#-------------------------------------------------------------------------------
# Copyright (c) 2022 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualisation Group.
#-------------------------------------------------------------------------------

variables:
  GIT_STRATEGY: none                # we disable fetch, clone or checkout for every job
  GIT_CHECKOUT: "false"             # as we only want to checkout and fetch in the preperation stage
  GIT_SUBMODULE_STRATEGY: "recursive"
  GIT_CLEAN_FLAGS: none
  GIT_DEPTH: "1"
  NUMBER_OF_OLD_VERSIONS: 3         # number of old revisions to keep after the deploy
  CLUSTER_ADDRESS: "ngm01.hpc.itc.rwth-aachen.de" # Needs to be ngm01 as all other nodes require MFA
  SHARED_META_URL: "https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/unreal-ci/-/raw/master/shared.meta"
  UNREAL_VERSION: "5.3"
  CUSTOM_NDISPLAY_CONFIG: ""


##################################Generate Project##################################

.Generate_Project_:
  stage: generate
  rules: 
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  tags:
    - linux
    - docker-executor
  image:
    name: registry.git-ce.rwth-aachen.de/vr-vis/vr-group/alpine-ci-container/alpine-ci:latest
  variables: 
    GEN_TEMPLATE_REPO: "https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/unrealprojecttemplate.git"
    GEN_TEMPLATE_BRANCH: "$UNREAL_VERSION"
    GEN_DEPENDENCIES: "()"
    RUN_SETUP: "true"
  script:
    - find ${CI_PROJECT_DIR} -mindepth 1 -delete
    - declare -A dependencies=$GEN_DEPENDENCIES
    - git clone -b "${GEN_TEMPLATE_BRANCH}" --depth 1 --single-branch --recursive ${GEN_TEMPLATE_REPO} ${CI_PROJECT_DIR}
    - if [[ "${RUN_SETUP}" == "true" ]] ; then
    -   bash ${CI_PROJECT_DIR}/setup.sh --install-default-plugins
    - fi
    - git submodule add https://gitlab-ci-token:${CI_JOB_TOKEN}@git-ce.rwth-aachen.de/${CI_PROJECT_PATH}.git/ Plugins/${CI_PROJECT_NAME}
    - cd ${CI_PROJECT_DIR}/Plugins/${CI_PROJECT_NAME}
    - if [[ $CI_PIPELINE_SOURCE == "merge_request_event" ]] ; then
    -   git fetch origin $CI_MERGE_REQUEST_REF_PATH
    - fi
    - git checkout ${CI_COMMIT_SHA}
    - cd ${CI_PROJECT_DIR}
    - for i in "${!dependencies[@]}"; do
    -   git submodule add -b "${i%%@*}" ${dependencies[$i]} Plugins/${i#*@}
    - done 
    - git remote rm origin && git -c user.name='project-generation-bot' -c user.email='support@vr.rwth-aachen.de' commit -am "Generated Project"
  artifacts:
    name: "Generated_Project_${CI_PROJECT_NAME}"
    paths: ["*"]
    expire_in: 3 days
    when: always

##################################Win64##################################

.Build_Windows_:
  stage: build
  variables:
    GIT_STRATEGY: clone
    GIT_CHECKOUT: "true"
    CLIENT_CONFIG: "Shipping"
    EXTRA_ARGS: ""
  tags:
    - windows
    - unreal
  script:
    # Clean folder before building
    - git clean -qxfdf; git submodule foreach --quiet --recursive git clean -qxfdf
    # Preparation and building stage
    - $env:Path="$env:PREPEND_PATH;$env:Path;$env:APPEND_PATH"
    - $env:PROJECT_NAME=(Get-ChildItem *.uproject).BaseName
    - . "$env:UE_ENGINES_FOLDER\UE_$env:UNREAL_VERSION\Engine\Build\BatchFiles\RunUAT.bat" BuildCookRun -Project="$env:CI_PROJECT_DIR/$env:PROJECT_NAME.uproject" -WaitMutex -NoP4 -TargetPlatform=Win64 -ClientConfig="$env:CLIENT_CONFIG" -Cook -allmaps -Build -Stage -Pak -Iterate -utf8output -Archive -ArchiveDirectory="$env:CI_PROJECT_DIR/Release/$env:CI_PROJECT_NAME" -Rocket -Package ($env:EXTRA_ARGS); $env:EXIT_CODE=$LASTEXITCODE
    # Copy build logs
    - mkdir -f "$env:CI_PROJECT_DIR/Release/$env:CI_PROJECT_NAME/BuildLogs/" | out-null
    - cp -ErrorAction Ignore -R "$env:UE_ENGINES_FOLDER\UE_$env:UNREAL_VERSION\Engine\Programs\AutomationTool\Saved\Logs\*" "$env:CI_PROJECT_DIR/Release/$env:CI_PROJECT_NAME/BuildLogs/" ; $true | out-null
    # Rename thumbnail if it exists
    - mv -ErrorAction Ignore ${PROJECT_NAME}.png thumbnail.png ; $true | out-null
    # Build variables
    - Add-Content -Path build_vars.env -Value "CLIENT_CONFIG=$env:CLIENT_CONFIG"
    - Add-Content -Path build_vars.env -Value "BUILD_CI_RUNNER_DESCRIPTION=$env:CI_RUNNER_DESCRIPTION"
    - Add-Content -Path build_vars.env -Value "BUILD_CI_RUNNER_TAGS=$env:CI_RUNNER_TAGS"
    - Add-Content -Path build_vars.env -Value "BUILD_CI_RUNNER_VERSION=$env:CI_RUNNER_VERSION"
    # Exit with exit code of the build
    - exit $env:EXIT_CODE
  timeout: 3h
  artifacts:
    name: "Packaged_Windows_$CI_PROJECT_NAME"
    paths:
      - Release/*/*
      - Saved/AutoScreenshot.png
      - thumbnail.png
      - launcher.meta
    reports:
      dotenv: build_vars.env
    expire_in: 3 days
    when: always
        
##################################Linux##################################

.Build_Linux_:
  image: ghcr.io/epicgames/unreal-engine:dev-$UNREAL_VERSION
  stage: build
  variables:
    GIT_STRATEGY: clone
    GIT_CHECKOUT: "true"
    CLIENT_CONFIG: "Shipping"
    EXTRA_ARGS: ""
  tags:
    - linux
  script:
    # Check for legacy tags and explicitly fail if they are still being used. 
    - if [[ $CI_RUNNER_TAGS = *unreal-* ]] ; then
    -   echo -e "\e[31mERROR You are still using Unreal-Version specific runner tags (unreal-XX). These are deprecated, please use the general <unreal> tag!"
    -   echo -e "\e[31mERROR To specify the Unreal Version, change the UNREAL_VERSION variable in your .gitlab-ci.yml!"
    -   exit 1
    - fi
    # Clean folder before building
    - git clean -qxfdf && git submodule foreach --quiet --recursive git clean -qxfdf
    # Preparation and building stage
    - PATH=$PREPEND_PATH:$PATH:$APPEND_PATH
    - export PROJECT_NAME=`basename $(find *.uproject) .uproject`
    # Check for matching unreal version
    - ASSOCIATION_LINE=$(grep "EngineAssociation" "${CI_PROJECT_DIR}/${PROJECT_NAME}.uproject")
    - ASSOCIATION=$(echo "$ASSOCIATION_LINE" | cut -d ":" -f 2 | cut -d "\"" -f 2 | cut -d "\"" -f 1)
    - if [[ $ASSOCIATION = [0-9]*\.[0-9]* && "$ASSOCIATION" != "$UNREAL_VERSION" ]]; then
    -   echo -e "\e[33mWARNING The Unreal Engine version on the runner does not match your project version.\nProject version:\t${ASSOCIATION}\nRunner version:\t\t${UNREAL_VERSION}\e[m"
    - fi
    - EXIT_CODE=0
    - (${UE_ENGINE_FOLDER}/Build/BatchFiles/RunUAT.sh BuildCookRun -project="${CI_PROJECT_DIR}/${PROJECT_NAME}.uproject" -WaitMutex -nop4 -targetplatform=Linux -clientconfig=${CLIENT_CONFIG} -cook -allmaps -build -stage -pak -iterate -utf8output -archive -archivedirectory="${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}" -rocket -package ${EXTRA_ARGS}) || EXIT_CODE=$?
    # Copy build logs
    - mkdir -p "${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/BuildLogs/"
    - cp -R "${ADDITIONAL_LOGS}/"* "${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/BuildLogs/" || true
    # Rename thumbnail if it exists
    - mv ${PROJECT_NAME}.png thumbnail.png || true
    # Build variables
    - echo "CLIENT_CONFIG=${CLIENT_CONFIG}" >> build_vars.env
    - echo "BUILD_CI_RUNNER_DESCRIPTION=${CI_RUNNER_DESCRIPTION}" >> build_vars.env
    - echo "BUILD_CI_RUNNER_TAGS=${CI_RUNNER_TAGS}" >> build_vars.env
    - echo "BUILD_CI_RUNNER_VERSION=${CI_RUNNER_VERSION}" >> build_vars.env
    # Exit with exit code of the build
    - exit ${EXIT_CODE}
  timeout: 3h
  artifacts:
    name: "Packaged_Linux_${CI_PROJECT_NAME}"
    paths:
      - Release/*/*
      - Saved/AutoScreenshot.png
      - thumbnail.png
      - launcher.meta
    reports:
      dotenv: build_vars.env
    expire_in: 3 days
    when: always
        
##################################Deploy##################################

.Deploy_General_:
  stage: deploy
  tags:
    - linux
    - docker-executor
  image:
    name: registry.git-ce.rwth-aachen.de/vr-vis/vr-group/alpine-ci-container/alpine-ci:latest
  allow_failure: true
  variables:
    SYNC_KEY: ""
    USER: ""
    GROUP: ""
    CREATE_LAUNCHER_META: "true"
  script:
    # Change access rights for SSH key to use it later on
    - chmod 600 ${SYNC_KEY}
    - chmod 600 ${SSH_KEY_CLEANUP_VRDEMO}
    # Find out if it was Windows, WindowsNoEditor, Linux or LinuxNoEditor
    - export PLATFORM_NAME=$(find ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/ -mindepth 1 -maxdepth 1 -type d -name "*Windows*" -or -name "*Linux*" | head -1 | xargs basename)
    # Copy Thumbnails if they exist
    - cp "${CI_PROJECT_DIR}/Saved/AutoScreenshot.png" "${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/thumbnail.png" 2> /dev/null || true
    - cp "${CI_PROJECT_DIR}/thumbnail.png" "${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/thumbnail.png" 2> /dev/null || true
    # RSync to Cluster
    - rsync -e "ssh -i ${SYNC_KEY}" --archive --info=progress2 --mkpath --human-readable --no-inc-recursive --chown=${USER}:${GROUP} --chmod=Dg+s,ug+rwx,o-rwx ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/ ${USER}@${CLUSTER_ADDRESS}:/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}/${CI_PIPELINE_IID}/
    # Clean up old builds
    - ssh -i ${SSH_KEY_CLEANUP_VRDEMO} ${USER}@${CLUSTER_ADDRESS} "/home/vrdemo/scripts/maintenance/remove_old_builds_for_project_ssh.sh ${CI_PROJECT_NAME}"
    # Generate launcher.meta
    - if [ -f ${CI_PROJECT_DIR}/launcher.meta ] && [[ "${CREATE_LAUNCHER_META}" == "true" ]] ; then
      # Define some variables which will be used in the generated launcher.meta
    -   export CI_DEPLOY_TIME=$(date +%Y_%m_%d_%H_%M)
    -   export CLEANED_COMMIT_MSG=$(echo ${CI_COMMIT_MESSAGE} | jq -cRM | tail -c +2 | head -c -2)
    -   export CLEANED_BUILD_CI_RUNNER_TAGS=$(echo ${BUILD_CI_RUNNER_TAGS} | jq -rc '. | join(", ")')
        # Substitute the values into place
    -   export SHARED_META=$(wget -qO- ${SHARED_META_URL} | head -n -1 | tail -n +2)
    -   cat ${CI_PROJECT_DIR}/launcher.meta | sed '/CI-DATA-ENTRIES/c\${SHARED_META}' | envsubst > ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/launcher.meta.tmp
    -   cat ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/launcher.meta.tmp | envsubst > ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/launcher.meta
    -   rm -f ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/launcher.meta.tmp

        # RSync launcher.meta last to allow launch only after sync is done
    -   rsync -e "ssh -i ${SYNC_KEY}" --archive --info=progress2 --mkpath --human-readable --no-inc-recursive --chown=${USER}:${GROUP} --chmod=0770 ${CI_PROJECT_DIR}/Release/${CI_PROJECT_NAME}/${PLATFORM_NAME}/launcher.meta ${USER}@${CLUSTER_ADDRESS}:/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}/${CI_PIPELINE_IID}/${PLATFORM_NAME}/
    - fi
    

.Deploy_VRDev_:
  extends: .Deploy_General_
  variables:
    SYNC_KEY: "${SSH_KEY_RSYNC_VRDEV}"
    USER: "vrdev"
    GROUP: "av00"
    CREATE_LAUNCHER_META: "false"
        
.Deploy_CAVE_:
  extends: .Deploy_General_
  variables:
    SYNC_KEY: "${SSH_KEY_RSYNC_CAVE}"
    USER: "av006de"
    GROUP: "av00"
    CREATE_LAUNCHER_META: "true"

.Deploy_vrdemo_:
  extends: .Deploy_General_
  variables:
    SYNC_KEY: "${SSH_KEY_RSYNC_VRDEMO}"
    USER: "vrdemo"
    GROUP: "vrdemo"
    CREATE_LAUNCHER_META: "true"
